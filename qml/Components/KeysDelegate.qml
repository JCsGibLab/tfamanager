/*
*/

import QtQuick 2.9
import Ubuntu.Components 1.3

//TODO: Avoid loading this on every element
//try send signals for delete and refresh
import QtQuick.LocalStorage 2.0
import "../js/db.js" as KeysDB

ListItem {
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    divider.visible: false
    clip: true
    highlightColor: root.highlightColor

    onClicked: {
        mainStack.push(Qt.resolvedUrl("PageKeysInfo.qml"), {
            "key": authkey,
            "id": id,
            "reminingTime": bar.value,
            "secret": secret,
            "url": url,
            "desc": desc
        });

        root.leftSearchView();
    }

    leadingActions: ListItemActions {
        actions: Action {
            iconName: "delete"
            text: i18n.tr("Delete")
            onTriggered: {
                KeysDB.deleteKey(id);
                root.initDB();
            }
        }
    }

    trailingActions: ListItemActions {
        actions: [
            Action {
                enabled: authkey.period == undefined
                visible: enabled
                iconName: "view-refresh"
                onTriggered: {
                    var difference =  1;
                    KeysDB.updateCounter(id, difference);
                    root.initDB();
                }
            },
            Action {
                iconName: "edit-copy"
                text: i18n.tr("Copy key")
                onTriggered: {
                    Clipboard.push(root.formatCode(layout.title.text, false))
                }
            },
            Action {
                iconName: "edit"
                text: i18n.tr("Edit key")
                onTriggered: {
                    mainStack.push(Qt.resolvedUrl("PageEditAddKey.qml"), {
                        "editMode": true,
                        "uriToEdit": url,
                        "descriptionToEdit": desc,
                        "idToUpdate": id
                    });

                    root.leftSearchView();
                }
            }
        ]
    }

    ListItemLayout {
        id:layout

        title.text: authkey.counter == undefined
            ? root.formatCode(OTPAuth.TOTP.generate(authkey), true)
            : root.formatCode(OTPAuth.HOTP.generate(authkey), true);
        title.font.bold: true

        subtitle.text: desc == "" || desc == " "
            ? i18n.tr("%1 at %2").arg(authkey.label).arg(authkey.issuer)
            : i18n.tr("%1. %2 at %3").arg(desc).arg(authkey.label).arg(authkey.issuer)

        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: currentCounter
            SlotsLayout.position: SlotsLayout.Trailing
            text: authkey.counter == undefined
                ? ""
                : i18n.tr("# %1").arg(authkey.counter)
            color: theme.palette.normal.baseText
        }

        ProgressionSlot {}

        //Let's connect with the main ticker
        Connections {
            target: timer

            onTriggered: {
                layout.title.text = authkey.counter == undefined
                    ? root.formatCode(OTPAuth.TOTP.generate(authkey), true)
                    : root.formatCode(OTPAuth.HOTP.generate(authkey), true);

                bar.value = timer.time % authkey.period
            }
        }
    }

    ProgressBar {
        id: bar
        visible: authkey.counter == undefined
        width: parent.width
        anchors.bottom: parent.bottom

        maximumValue: authkey.period || 0
        minimumValue: 0
        value: timer.time % authkey.period
    }
}
