/*
 * Copyright (C) 2019 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Item {
    height: lengSetting.height

    property string selectedLeng: "6"

    ListModel {
        id: lengModel
        Component.onCompleted: initialize()

        function initialize() {
            lengModel.append({"text": "6"})
            lengModel.append({"text": "7"})
            lengModel.append({"text": "8"})
            lengModel.append({"text": "9"})
        }
    }

    ExpandableListItem {
        id: lengSetting
        listViewHeight: lengModel.count*units.gu(6.1)
        model: lengModel
        title.text: selectedLeng

        delegate: StandardListItem {
            title.text: model.text
            icon.name: "ok"
            icon.visible: selectedLeng === model.text

            onClicked: {
                selectedLeng = model.text;
                lengSetting.toggleExpansion();
            }
        }
    }
}
