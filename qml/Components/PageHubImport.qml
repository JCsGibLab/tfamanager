import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

Page {
    id: importPage
    objectName: "importHubPage"

    property var activeTransfer
    property bool allowMultipleFiles

    signal accept(var files)
    signal close()

    header: HeaderBase {
        title: i18n.tr("Import backup file from")

        leadingActionBar.actions: Action {
            iconName: "back"
            text: i18n.tr("Back")

            onTriggered: close();
        }
    }

    Rectangle {
        anchors.fill: parent
        anchors.topMargin: importPage.header.height

        ContentTransferHint {
            anchors.fill: parent
            activeTransfer: importPage.activeTransfer
        }

        ContentPeerPicker {
            visible: importPage.visible
            showTitle: false
            contentType: ContentType.Documents
            handler: ContentHandler.Source

            onPeerSelected: {
                peer.selectionType = importPage.allowMultipleFiles
                    ? ContentTransfer.Multiple
                    : ContentTransfer.Single;

                importPage.activeTransfer = peer.request();
                stateChangeConnection.target = importPage.activeTransfer;
            }

            onCancelPressed: {
                close();
            }
        }
    }

    Connections {
        id: stateChangeConnection
        target: null

        onStateChanged: {
            var statesForDebug = ["Created","Initiated","InProgress","Charged","Collected","Aborted","Finalized","Downloading","Downloaded"]
            console.log("DEBUG: activeTrasfer State is", statesForDebug[activeTransfer.state])

            switch (activeTransfer.state) {
                case ContentTransfer.Aborted:
                  console.log("Aborted")
                  break;
                case ContentTransfer.Collected:
                  console.log("Finalize and close")
                  activeTransfer.finalize();
                  close();
                  break;
                case ContentTransfer.Finalized:
                  console.log("Finalized")
                  break;
                case ContentTransfer.InProgress:
                  console.log("InProgress")
                  break;
                case ContentTransfer.Charged:
                  var selectedItems = []

                  for (var i in importPage.activeTransfer.items) {
                    //console.log("Move imported items",importPage.activeTransfer.items[i].move(backupPage.cachePath))
                    selectedItems.push(
                        String(importPage.activeTransfer.items[i].url) //Don't remove .replace("file://", "")
                    );
                    console.log("SEEE",selectedItems[i])
                  }

                  accept(selectedItems);
                  break;
                default:
                  console.log("Other state?",activeTransfer.state)
            }
        }
    }
}
