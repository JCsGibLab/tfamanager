/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Rectangle {
    anchors.horizontalCenter: parent.horizontalCenter
    color: theme.palette.normal.background

    ProgressBar {
        visible: parent.visible
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter
        indeterminate: true
    }
}
